from gamelib import Game, SpriteEntity, LabelEntity, play_sound
import random
import os
import math

files = os.listdir("assets/resized")


class Watts(LabelEntity):
    def __init__(self, game: Game, text: str = "", layer: int = 0, **kwargs):
        super().__init__(game, text, layer, **kwargs)

        self.x = 92.5
        self.y = 40
        self.watts = 0
        self.font_size = 30
        
    def update(self, dt):
        self.watts = self.game.connector.watts
        self.text = f"{self.watts} watts"


class Points(LabelEntity):
    def __init__(self, game: Game, text: str = "", layer: int = 0, **kwargs):
        super().__init__(game, text, layer, **kwargs)

        self.x = 100
        self.y = 100
        self.font_size = 30
        

    def update(self, dt):
        self.text = f"{self.game.points} points"

class LifeCount(LabelEntity):
    def __init__(self, game: Game, text: str = "", layer: int = 0, **kwargs):
        super().__init__(game, text, layer, **kwargs)

        self.x = 80
        self.y = 140
        self.font_size = 30
        
    def update(self, dt):
        self.text = f"{self.game.life_count} Lifes"

class Fruit(SpriteEntity):
    GRAVITY = 9.81

    def __init__(
        self, game, image=None, half="", velocity_x=400, time: float = 0, **kwargs
    ):
        if not image:
            image = f"assets/resized/{random.choice(files)}"

        super().__init__(game, image, half=half, layer=1, **kwargs)
        self.half = half
        self.velocity_y = 15
        self.time = time
        if not self.half:
            self.direction = random.choice(["left", "right"])

            if self.direction == "left" and not half:
                self.x = 0
                self.velocity_x = 400  # Initial horizontal velocity
            else:
                self.x = game.width
                self.velocity_x = -400  # Initial horizontal velocity
            self.y = game.height * 0.9
        else:
            self.velocity_x = velocity_x

        if self.half == "":  # noqa: PLC1901
            play_sound("assets/sounds/throw.mp3")

    def update(self, dt):
        self.x += self.velocity_x * dt
        self.y += self.velocity_y * dt - (0.5 * Fruit.GRAVITY * math.pow(self.time, 2))
        self.time += dt
    
        in_hit_box = (
            self.x > self.game.width * 1 / 3 and self.x < self.game.width * 2 / 3
        )
        
        if self.y + self.height < 0 and not self.half:
            play_sound("assets/sounds/fall.mp3")
            self.game.life_count -= 1
            self.remove()
            return
        
        # self.game.connector.pulses > 10
        slice_fruit = "SPACE" in self.game.keys_down or self.game.connector.pulses > 10
        if not self.half and in_hit_box and slice_fruit:
            sounds = ["chop", "knife"]
            
            self.game.points += int(self.image_file[15:17])
            play_sound(f"assets/sounds/{random.choice(sounds)}.mp3")

            for index in range(2):
                half = "right" if index == 0 else "left"
                x = self.x + 10 if half == "left" else self.x - 10
                velocity_x = self.velocity_x if half == "left" else self.velocity_x * -1

                Fruit(
                    self.game,
                    self.image_file,
                    time=self.time,
                    half=half,
                    x=x,
                    y=self.y,
                    velocity_x=velocity_x,
                )
            self.remove()

class BackGround(SpriteEntity):
    def __init__(self, game):

        super().__init__(game, "assets/background.jpg", layer=-1, left=0, bottom=0)

class Menu(SpriteEntity):
    def __init__(self, game: Game, **kwargs):
        super().__init__(game, image_file="assets/menu-bg.png", layer=1, left=0, bottom=0, **kwargs)


class MenuStart(SpriteEntity):
    def __init__(self, game, image_file):
        super().__init__(game, image_file=image_file, layer=4, x=game.width/2, y=game.height/2)
        
        self.opacity = 255
        self.__fade_direction = "down"
        
    def update(self, dt: float):  # noqa: ARG002
        if self.opacity == 255:
            self.__fade_direction = "down"
        if self.opacity == 0:
            self.__fade_direction = "up"
        
        self.opacity += 5 if self.__fade_direction == "up" else -5
        
        if "SPACE" in self.game.keys_down or self.game.connector.pulses > 10:
            self.game.start(restart=True)

class GameOverStart(SpriteEntity):
    def __init__(self, game, image_file):
        super().__init__(game, image_file=image_file, layer=4, x=game.width/2, y=100)
        self.opacity = 255
        self.__fade_direction = "down"
        
    def update(self, dt: float):  # noqa: ARG002
        if self.opacity == 255:
            self.__fade_direction = "down"
        if self.opacity == 0:
            self.__fade_direction = "up"
        
        self.opacity += 5 if self.__fade_direction == "up" else -5
        
        if "SPACE" in self.game.keys_down or self.game.connector.pulses > 10:
            self.game.start(restart=True)

class GameOver(SpriteEntity):
    def __init__(self, game):
        super().__init__(game, 'assets/game_over.jpg', layer=3, left=0, bottom=0)


class RowerGame(Game):
    def __init__(self, connector):
        self.__next_fruit_time = 0
        self.connector = connector
        self.points = 0
        self.life_count = 3
        super().__init__(1200, 750)

    def start(self, restart=False):
        self.__playing = False
        Menu(self)
        MenuStart(self, image_file='assets/row-to-start.png')
        if restart:
            self.clear_entities()
            self.__playing = True
            self.life_count = 5
            BackGround(self)
            Watts(self)
            Points(self)
            LifeCount(self)
    

    def update(self, dt):

        self.__next_fruit_time -= dt
        self.connector.connection.requestStatistic("watts")
        
        initial_spawn_time = 4
        decay_rate = 0.01  # Adjust this value to control the rate of decay

# Calculate the adjusted spawn time based on the player's score
        adjusted_spawn_time = max(initial_spawn_time - decay_rate * self.points, 1)
        spawn_time = round(adjusted_spawn_time)
            
        time_between_slices = spawn_time
        
        
        if self.__next_fruit_time < 0 and self.__playing:
            Fruit(self)
            self.__next_fruit_time = time_between_slices

        if self.life_count == 0:
            GameOver(self)
            GameOverStart(self, image_file="assets/row-to-try-again.png")
            self.__playing = False
            
