from PIL import Image

def crop_image_half(image_path, output_path):
    # Open the image
    original_image = Image.open(image_path)
    
    # Get the dimensions of the original image
    width, height = original_image.size
    
    # Calculate the box coordinates for cropping
    box = (0, 0, width / 2, height)
    
    # Crop the image
    cropped_image = original_image.crop(box)
    
    # Save the cropped image
    cropped_image.save(output_path)

# Example usage
input_image_path = "assets/resized/68banana.png"  # Replace with your image file path
output_image_path = "assets/cut/banana-left.png"  # Replace with the desired output path
crop_image_half(input_image_path, output_image_path)
