# This allows the use of circular dependencies in type hints.
from __future__ import annotations
from typing import Any
import pyglet
from abc import ABC, abstractmethod
from pyglet import image


def play_sound(file_name: str):
    """Looks for the sound files loaded to the pyglet media or
    loads it to pyglet if wasn't loaded before, and stores is as a sound media object
    - plays the sound object to actually play the sound in the game

    Args:
        file_name (str): _description_
    """
    sound = pyglet.media.load(file_name, streaming=False)
    sound.play()


class Entity:
    """A general game object, providing a base class structure for different types of game objects"""

    def __init__(self, game, drawable, **kwargs):
        """
        Args:
            game: The `Game` that the created entity should be registered with, can take any game class
            drawable (_type_): can take any drawable object, that will be later "drawn" on the game window

        Raises:
            ValueError: raises a value error in case an attribute that doesn't exist was passed in **kwargs
        """
        self.__game = game
        self._drawable = drawable
        """pyglet drawable object. contains the attributes for the entity that will
        eventually 'drawn' on the game window """

        # Add us to the (semi-private) list of entities.
        game._entities.add(self)  # noqa: SLF001 # pyright: ignore

        # Try to apply any additional keyword arguments as attributes.
        for name, value in kwargs.items():
            if not hasattr(self, name):
                raise ValueError(
                    f"'{self.__class__.__name__}' has no '{name}' attribute"
                )
            setattr(self, name, value)

    @property
    def game(self) -> Any:
        return self.__game

    def remove(self):
        """- removes itself from the Game class entities set.
        - sets the game attribute to be not linked to any instance of a game
        - clears the entity's batch
        - completely removes the sprite entity from video memory.
        """
        if self.__game:
            self.__game._entities.remove(self)  # noqa: SLF001 # pyright: ignore
            self.__game = None
            self._drawable.batch = None
            self._drawable.delete()

    def update(self, dt: float):
        """abstract method of the built in update function coming from pyglet.
        the methods is constantly running in a loop.
        Can be dynamically modified per Class to react to different game scenarios

        Args:
            dt (float): delta time -  built in argument passed from pyglet.
            Equals to the elapsed time since the last update method that was executed
        """

    def collides_with(self, other: Entity, overlap: int = 0):
        """checks if the passed entity has colided on the screen with the "self" entity

        Args:
            other (Entity): A different entity in the game that could collide with this entity
            overlap (int, optional): Allows to specify how many px of overlap are required for the entities to be considered "collided".
            Defaults to 0.

        Returns:
            bool
        """
        return (
            self.right > other.left + overlap
            and self.left + overlap < other.right
            and self.top > other.bottom + overlap
            and self.bottom + overlap < other.top
        )

    @property
    def x(self) -> float:
        """Horizontal position of the center of the entity relative to the left border of the window in pixels."""
        # Note how `@property`s are named and documented as if they were *things* instead of *actions*.
        return self._drawable.x

    @x.setter
    def x(self, value: float):
        # There's no need to document setters, as they're conceptually the same thing as their getter.
        self._drawable.x = value

    @property
    def y(self) -> float:
        """Vertical position of the center of the entity relative to the bottom of the window in pixels."""
        return self._drawable.y

    @y.setter
    def y(self, value: float):
        self._drawable.y = value

    @property
    def opacity(self) -> int:
        """Blend opacity. An opacity of 255 means full opaque (solid), 128 means
        semi-transparent and 0 means fully invisible. Defaults to 255."""
        return self._drawable.opacity

    @opacity.setter
    def opacity(self, value: int):
        self._drawable.opacity = value

    @property
    def width(self) -> int:
        """Object width in px

        Returns:
            int: The width of the Entity(px)
        """
        return self._drawable.width

    @property
    def height(self) -> int:
        """
        Returns:
            int: The height of the Entity(px)
        """
        return self._drawable.height

    @property
    def top(self) -> float:
        """Vertical position of the top of the entity relative to the bottom of the window in pixels."""
        return self.y + self.height / 2

    @top.setter
    def top(self, value: float):
        self.y = value - self.height / 2

    @property
    def bottom(self) -> float:
        """Vertical position of the bottom of the entity relative to the top of the window in pixels."""
        return self.y - self.height / 2

    @bottom.setter
    def bottom(self, value: float):
        self.y = value + self.height / 2

    @property
    def left(self) -> float:
        """Horizontal position of the left of the entity relative to the right of the window in pixels."""
        return self.x - self.width / 2

    @left.setter
    def left(self, value: float):
        self.x = value + self.width / 2

    @property
    def right(self) -> float:
        """Horizontal position of the right of the entity relative to the left of the window in pixels."""
        return self.x + self.width / 2

    @right.setter
    def right(self, value: float):
        self.x = value - self.width / 2


class SpriteEntity(Entity):
    """A game object composed of an image that's displayed somewhere on the screen,
    optionally scaled, rotated and semi-transparent,"""

    __images_by_name: dict[str, pyglet.image.AbstractImage] = {}

    def __init__(self, game: Game, image_file: str, layer: int = 0, half="", **kwargs):
        """Create a new `SpriteEntity`, loading the image from the given file name, and
        register it with the `Game`.

        Args:
            game (Game): The `Game` that the created entity should be registered with.
            image_file (str): The name of the image file to use.
            layer (int, optional): The layer on which to draw this entity. Layers with higher
                numbers are drawn in front of layers with lower numbers. Defaults to 0.
        """

        drawable = pyglet.sprite.Sprite(
            self.__load_image(image_file, half),
            batch=game._get_batch(),  # type: ignore  # noqa: SLF001
            group=game._get_group(layer),  # type: ignore  # noqa: SLF001
        )
        super().__init__(game, drawable, **kwargs)
        self.__image_file = image_file

    @classmethod
    def __load_image(cls, file_name: str, half=""):
        """checks weather the image already stored in the class dictionary attribute: __images_by_name.
            if not then loads the image as a pyglet image and stores it as a static attribute
        Args:
            file_name (str): image file path

        Returns:
            image (pyglet.image.ImageData): returns a pyglet ImageData class object
            containing the dimensions of the image and its formatting
        """
        if half == "left" or half == "right":
            full_image = pyglet.image.load(file_name)
            # Calculate the dimensions of half of the image
            half_width = full_image.width // 2
            half_height = full_image.height

            # Create an image region representing the left half of the image
            image = (
                full_image.get_region(0, 0, half_width, half_height)
                if half == "left"
                else full_image.get_region(half_width, 0, half_width, half_height)
            )

        else:
            image = cls.__images_by_name.get(file_name)
            if not image:
                image = cls.__images_by_name[file_name] = pyglet.image.load(file_name)
                image.anchor_x = image.width // 2
                image.anchor_y = image.height // 2
        return image

    @property
    def image_file(self) -> str:
        """Get method for the image file"""
        return self.__image_file

    @image_file.setter
    def image_file(self, file_name: str):
        self.__image_file = file_name
        self._drawable.image = self.__load_image(file_name)

    @property
    def rotation(self) -> float:
        """Used to rotate the entity clockwise.
        Returns:
            float: Rotation in degrees.
        """
        return self._drawable.rotation

    @rotation.setter
    def rotation(self, value: float):
        self._drawable.rotation = value

    @property
    def scale(self) -> float:
        """Object size scale determined by a factor. The base scale is 1.
        Leaving the object in its original size

        Returns:
            float: factor of scale(base=1)
        """
        return self._drawable.scale

    @scale.setter
    def scale(self, value: float):
        self._drawable.scale = value


class ColorEntity(Entity):
    """An abstract class to build from different types of colored game objects.
    adds the the color attribute to the Entity class

    """

    @property
    def color(self) -> tuple[int]:
        """Getter for the color attribute

        Returns:
            tuple[int]: color code in rgb()
        """
        return self._drawable.color

    @color.setter
    def color(self, value: tuple[int, ...] | str):
        """takes the color code as a str and turns it into rgba() tuple
            if passed already has rgb then skips the conversion and simply set the new color
            value.

        Args:
            value (tuple[int, ...] | str): color code as rgba() or a as # string
        """
        if isinstance(value, str):
            # A CSS-style color string like `#ef6b00` or `#ef6b0080`. Convert to (r,g,b,a,) tuple.
            if value[0] == "#":
                value = value[1:]  # Strip the hash
            if len(value) == 6:
                value += "FF"  # Add the opacity
            assert len(value) == 8
            value = tuple(int(value[i : i + 2], 16) for i in range(0, 8, 2))
        self._drawable.color = value


class LabelEntity(ColorEntity):
    """A text entity allowing you to add headers and text to the game"""

    def __init__(self, game: Game, text: str = "", layer: int = 0, **kwargs):
        """Creates a new label entity and loading it to the game

        Args:
            game (Game): the current game object
            text (str, optional): optional text to insert to the game. Defaults to "".
            layer (int, optional): layer on the game window, choose if to put above or behind
            different objects in the game. Defaults to 0.
        """
        drawable = pyglet.text.Label(
            text,
            batch=game._get_batch(),
            group=game._get_group(layer),
            anchor_x="center",
            anchor_y="center",
        )  # SLF001 # pyright: ignore  # noqa: SLF001
        super().__init__(game, drawable, **kwargs)

    # We're overriding width and height here, as their values have a different meaning in pyglet labels.
    # The things we're after are called `content_width` and `content_height`.

    @property
    def width(self):
        return self._drawable.content_width

    @property
    def height(self):
        return self._drawable.content_height

    @property
    def font_name(self) -> str:
        """Add specific font styles by specifying the font name as a string

        Returns:
            str: font name
        """
        return self._drawable.font_name

    @font_name.setter
    def font_name(self, value: str):
        self._drawable.font_name = value

    @property
    def font_size(self) -> int:
        """Font size in px

        Returns:
            int: font size(in px)
        """
        return self._drawable.font_size

    @font_size.setter
    def font_size(self, value: int):
        self._drawable.font_size = value

    @property
    def text(self) -> str:
        """The actual string text"""
        return self._drawable.text

    @text.setter
    def text(self, value: str):
        self._drawable.text = value

    @property
    def bold(self) -> bool:
        """Allows to set text to bold

        Returns:
            bool
        """
        return self._drawable.bold

    @bold.setter
    def bold(self, value: bool):
        self._drawable.bold = value

    @property
    def italic(self) -> bool:
        """Allows to turn text to italic style

        Returns:
            bool: _description_
        """
        return self._drawable.italic

    @italic.setter
    def italic(self, value: bool):
        self._drawable.italic = value


class RectEntity(ColorEntity):
    """A rectangular entity class created as a shape with pyglet"""

    def __init__(self, game: Game, layer: int = 0, **kwargs):
        """Creates a new rectangular shape as a game object
            we can specify width, height and location on the board to adjust
            the size of the shape and its location on the screen

        Args:
            game (Game): current game object
            layer (int, optional): Layer on the game window,
            pass a higher number to send to the front and vise versa. Defaults to 0.
        """

        drawable = pyglet.shapes.Rectangle(batch=game._get_batch(), group=game._get_group(layer), x=10, y=10, width=10, height=10)  # type: ignore  # noqa: SLF001
        drawable.anchor_x = 5
        drawable.anchor_y = 5
        super().__init__(game, drawable, **kwargs)

    @property
    def width(self) -> int:
        """Shapes width

        Returns:
            int: width (in px)
        """
        return self._drawable.width

    @width.setter
    def width(self, value):
        self._drawable.width = value
        self._drawable.anchor_x = value / 2

    @property
    def height(self) -> int:
        """Shape height

        Returns:
            int: height (in px)
        """
        return self._drawable.height

    @height.setter
    def height(self, value):
        self._drawable.height = value
        self._drawable.anchor_y = value / 2


class Game(ABC):
    """The actual game class as an abstract class to create different types of games.
       used to initiate the game and store all of the different entities in it.

    Args:
        ABC : python abc module for creating abstract classes
    """

    # game sound file sources
    __sounds: dict[str, pyglet.media.StaticSource] = {}
    # divide game objects by their layer on the window
    __layer_groups: dict[int, pyglet.graphics.Group] = {}

    def __init__(self, width: int, height: int, **kwargs):
        """Creates a new game instance and creates a new game window,
        depending on dimensions passed.

        args:
            width (int): game window width(px)
            height (int): game window height(px)
        """

        self._entities: set[Entity] = set()
        """A set if all the entities created in the game"""
        self.__batch = pyglet.graphics.Batch()
        """Tell pyglet to draw the game entities at once, as a batch"""

        self.keys_down: set[str] = set()
        """A set of textual representations for each of the keys that is currently being held down."""
        self.keys_pressed: list[str] = []
        """An ordered list of all keys that have been pressed since the last *update*."""

        # Create a pyglet window.
        self.__window = pyglet.window.Window(width, height, **kwargs)

        # Game-specific initialization.
        self.start()

        # Setup event handling methods.
        self.__window.on_key_press = self.__on_key_press  # type: ignore
        self.__window.on_key_release = self.__on_key_release  # type: ignore
        self.__window.on_draw = self.__on_draw  # type: ignore
        pyglet.clock.schedule_interval(self.__update, 1 / 60)  # max 60 fps
        """Schedule the interval of looping the game event listeners"""

    @abstractmethod
    def start(self):
        """Start the game method"""

    def update(self, dt):  # noqa: B027
        """Update loop to handle game scenarios"""

    @property
    def width(self):
        """Get method for game window width"""
        return self.__window.width

    @property
    def height(self):
        """Get method for game window height"""
        return self.__window.height

    def clear_entities(self):
        """In case of restarting the game, allows to remove all entities from the memory"""
        for entity in set(self._entities):
            entity.remove()

    def __update(self, dt: float):
        """Simplifies the update method to be activated for the game objects, as well
        as any other game entity.
        in every update loop it calls the update function for the game and for entity
        inside the game.
        - Clears the keys pressed static attribute to be empty.
        - Passes the delta time coming from pyglet to the individual update methods

        Args:
            dt (float): delta time
        """
        self.update(dt)
        for entity in set(self._entities):
            if entity.game:
                entity.update(dt)
        self.keys_pressed.clear()

    def get_entities_of_type(self, entity_type: type):
        """Get method for different entity classes types that inherit
        the Entity class

        Args:
            entity_type (type): the entity class type

        Returns:
            list[type]: a list of the different entities matching the type
        """
        return [entity for entity in self._entities if isinstance(entity, entity_type)]

    @staticmethod
    def run():
        """Actually runs the game using pyglet"""
        pyglet.app.run()

    def _get_batch(self) -> pyglet.graphics.Batch:
        """get

        Returns:
            pyglet.graphics.Batch object: contains the game objects batched entities
        """
        return self.__batch

    def _get_group(self, layer: int) -> pyglet.graphics.Group:
        """Creates a layer group if doesn't exist for the layer.
            and returns the group containing the amount of objects for the layer

        Args:
            layer (int): layer on the game window
        Returns:
            pyglet.group.Group: layer group
        """
        if layer not in self.__layer_groups:
            self.__layer_groups[layer] = pyglet.graphics.Group(layer)
        return self.__layer_groups[layer]

    def __on_key_press(self, symbol: int, _modifiers: int):
        """Gets triggered by the event handler we set before.
        - Converts the key pressed into its string representation("UP"/"DOWN"/"A" etc...), using pyglet.
        - Then appends it to a list of current pressed keys


        Args:
            symbol (int): the key symbol as key number code
            _modifiers (int): modifier key number code ( shift key, command/control keys, etc...)
        """
        name = pyglet.window.key.symbol_string(symbol)
        self.keys_down.add(name)
        self.keys_pressed.append(name)

    def __on_key_release(self, symbol: int, _modifiers: int):
        """Removes the released key from the keys down list"""

        name = pyglet.window.key.symbol_string(symbol)
        try:
            self.keys_down.remove(name)
        except KeyError:
            pass

    def __on_draw(self):
        """Clears to empty game window.
        Draws all of the drawable items(game entity objects on the window) saved in the game batch
        """
        self.__window.clear()
        self.__batch.draw()
