import time
import serial
import serial.tools.list_ports

from connection import WaterRowerConnection
import rower_ninja

class WaterRowerConnector:

    def __init__(self):
        self.port = None
        self.connection = None
        self.watts = 0
        self.pulses = 0

    def onDisconnect(self):
        self.connection = None

    def run(self):
        # Start monitoring in the background, calling onEvent when data comes in.
        self.connect()
        

        # This is where you run your main application. For instance, you could start a Flask app here,
        # run a GUI, do a full-screen blessed virtualization, or just about anything else.
        rower_ninja.RowerGame(self).run()

    def onEvent(self, event):
        """Called when any data comes."""
        print(event, flush=True)
        
        if event["type"] == "pulse":
            self.pulses = event['value']
            print(self.pulses)

        if event["type"] == "watts":
            self.watts = event["value"]

    def connect(self):
        """This will start a thread in the background, that will call the onEvent method whenever data comes in."""
        print("Connecting to WaterRower...")
        self.port = self.findPort()
        print("Connecting to WaterRower on port %s" % self.port)
        self.connection = WaterRowerConnection(
            self.port, self.onDisconnect, self.onEvent
        )

    def findPort(self):
        attempts = 0
        while True:
            attempts += 1
            ports = serial.tools.list_ports.comports()
            for path, name, _ in ports:
                if "WR" in name:
                    print("port found: %s" % path)
                    return path

            # message every ~10 seconds
            if attempts % 10 == 0:
                print("Port not found in %d attempts; retrying every 5s" % attempts)

            time.sleep(1)


if __name__ ==  '__main__':
    WaterRowerConnector().run()
